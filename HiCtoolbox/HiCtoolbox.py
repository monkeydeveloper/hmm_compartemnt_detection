#python 3
#2019-2020
#CC-By-SA
#Carron Leopold
#specific utils tools for hic need

import h5py
import numpy as np
from scipy import sparse


#####
#ADDITIONAL LOADER

def EpiGbyres(EpiGfilename,res,achr,sizeatres,NbEpi):
	"""
	Generate a matrix of repartition of epiG in a chr at the given resolution
	Do it the hard way : parse the file to the desired resolution directly
	"""
	Epimat=np.zeros((NbEpi,sizeatres))
	o=open(EpiGfilename,'r')
	l=o.readline()
	while l:
		ls=l.split()
		if ls[0]==achr:
			#force to scale bp cover in bin cover here
			EpiV=int(ls[3])-1
			b=np.float(ls[1])
			e=np.float(ls[2])
			begin=int(np.floor(b/res)) #force to cast for indexing error
			end=int(np.floor(e/res)) #force to cast for indexing error
			eC=end*res
			bC=begin*res
			val=1
			if begin==end: #1 bin
				Epimat[EpiV,begin]+=val*(e-b)
			else:
				if (end-begin)==1: #2 bin
					Epimat[EpiV,begin]+=val*(eC-b)
					Epimat[EpiV,end]+=val*(e-eC)
				else: #more than 2 bin
					Epimat[EpiV,begin]+=val*(res-(b-bC))
					while begin<end:
						Epimat[EpiV,begin]+=val*res
						begin+=1
					if (e-eC)>0:
						Epimat[EpiV,begin]+=val*(e-eC) #here begin=end
		l=o.readline()
	o.close()
	return Epimat

#####
#Binning tools
def bin2d(Data,p,q):    
    """   
    Data = input matrix
    p,q rescaling factors
    Written for sparse 
    """
    n,m=np.shape(Data);
    s=(int(np.ceil(n/p)),int(np.ceil(m/q)))
    i,j,d = sparse.find(Data);
    i=np.int_(np.ceil(i/p))
    j=np.int_(np.ceil(j/q))
    M=sparse.csr_matrix((d,(i,j)))
    return M



def bin1D(anumpyarray,resolutionfrom,resolutionto):
	"""
	in : A numpy array , number of bin in raw and in col
	out : the matrix binned
	"""
	print(resolutionto,resolutionfrom)
	if resolutionto>resolutionfrom:
		convertionfactor=np.ceil(resolutionto/resolutionfrom)
		s=anumpyarray.shape
		print("dimension du vecteur:",s)
		#has to be identical as result in other function like chrsizedict)
		newsizei=np.ceil(s[0]*resolutionfrom/resolutionto)
		newarray=np.zeros(int(newsizei))
		print("taille du vecteur appres rescale :",newarray.shape)
		i=0
		while i<newsizei:
			ifrom=int(i*convertionfactor)
			ito=int((i+1)*convertionfactor)
			if i==newsizei-1:
				asum=np.sum(anumpyarray[ifrom:])
			else:
				asum=np.sum(anumpyarray[ifrom:ito])
			newarray[i]=asum
			i+=1
		return newarray
	elif resolutionto==resolutionfrom:
		print("no binning")
		return anumpyarray
	else:
		print("wrong resolution parameter in bin1D")

def bin2dfullmat(anumpyarray,resolutionfrom,resolutionto):
	"""
	in : A numpy array , number of bin in raw and in col
	out : the matrix binned
	Written for full
	"""
	print('change of resolution from ',resolutionfrom,' to ',resolutionto)
	if resolutionto>resolutionfrom:
		convertionfactor=np.ceil(resolutionto/resolutionfrom)
		s=anumpyarray.shape
		print("Initial HiC size before binning:",s)
		#has to be identical as result in other function like chrsizedict)
		newsizei=np.ceil(s[0]*resolutionfrom/resolutionto)
		newsizej=np.ceil(s[1]*resolutionfrom/resolutionto)
		newarray=np.zeros((int(newsizei),int(newsizej)))
		print("HiC size after binning :",newarray.shape)
		i=0
		j=0
		while i<newsizei:
			while j<newsizej:
				ifrom=int(i*convertionfactor)
				ito=int((i+1)*convertionfactor)
				jfrom=int(j*convertionfactor)
				jto=int((j+1)*convertionfactor)
				if i==newsizei-1:
					asum=np.sum(anumpyarray[ifrom:,jfrom:jto])
				elif j==newsizej-1:
					asum=np.sum(anumpyarray[ifrom:ito,jfrom:])
				elif i==newsizei-1 and j==newsizej-1:
					asum=np.sum(anumpyarray[ifrom:,jfrom:])
				else:
					asum=np.sum(anumpyarray[ifrom:ito,jfrom:jto])
				newarray[i,j]=asum
				newarray[j,i]=asum
				j+=1
			i+=1
			j=0
		return newarray
	elif resolutionto==resolutionfrom:
		print("No binning")
		return anumpyarray
	else:
		print("Wrong resolution parameter")


#####
#operation tool

def SCN(D, max_iter = 10):
	"""
	Out  : SCN(D)
	Code version from Vincent Matthys
	"""    
	# Iteration over max_iter    
	for i in range(max_iter):        
		D /= np.maximum(1, D.sum(axis = 0))       
		D /= np.maximum(1, D.sum(axis = 1))    
		# To make matrix symetric again   
	return (D + D.T)/2 

def fastFloyd(contact):
	"""
	out : FF(contact)
	Code version from Vincent Matthys
	"""      
	n = contact.shape[0]    
	shortest = contact    
	for k in range(n):        
		i2k = np.tile(shortest[k,:], (n, 1))        
		k2j = np.tile(shortest[:, k], (n, 1)).T        
		shortest = np.minimum(shortest, i2k + k2j)    
	return shortest


def filteramat(Hicmat,Filterextremum=True,factor=1.5):
	"""
	in : a HiCmat without any transformation, factor of reduction
	out : the HiCmatreduce,thevector of his transformation
	THE filter part from the main in one function
	"""
	Hicmatreduce=Hicmat
	#first step : filter empty bin
	sumHicmat=Hicmat.sum(axis = 0)
	segmenter1=sumHicmat>0
	A=np.where(segmenter1)
	Hicmatreduce=Hicmatreduce[A[1],:]
	Hicmatreduce=Hicmatreduce[:,A[1]]
	if Filterextremum:
		#second step : filter lower bin
		sumHicmat=np.sum(Hicmatreduce,0)
		msum=np.mean(sumHicmat)
		mstd=np.std(sumHicmat)
		mini = msum-mstd*factor
		maxi = msum+mstd*factor
		#Make the bolean condition
		newcond=mini < sumHicmat
		newcond2=sumHicmat < maxi
		newcond=np.logical_and(newcond,newcond2)
		B=np.where(newcond)
		#Filter
		Hicmatreduce=Hicmatreduce[B[1],:]
		Hicmatreduce=Hicmatreduce[:,B[1]]
		segmenter1=A[1][B[1]] #Create the binsaved index
	return Hicmatreduce,segmenter1

#####
#Output WRITER


def writePDB(fnameout,value,EpiValue):
	"""
	write a PDB from value contain in value
	just bored to read 200 pages of biopython for a simple writing script
	I use tips from pierre poulain blog
	"""
	Sh=np.shape(value)
	#out
	fout=open(fnameout,'w')
	i=1
	while i<=Sh[0]:
		S="{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}          {:>2s}{:2s}" #PDB format in python
		#print("here",value[i-1,0],value[i-1,1],EpiValue[i-1])
		S=S.format('ATOM',i,'CA','','SOL','A',i,'',float(value[i-1,0]),float(value[i-1,1]),float(value[i-1,2]),1,float(EpiValue[i-1]),'O','')
		#print(S)
		fout.write(S+"\n")
		i+=1
	fout.close()

