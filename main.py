# python 3
# 2020
# CC-By-SA
# original code Carron Leopold, Julien Mozziconacci
# modified + extended by Matej Nemec et al.

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse
import pandas as pd
from sammon import sammon
import HiCtoolbox.HiCtoolbox as HiCtoolbox
from hmmlearn import hmm
from sklearn.decomposition import PCA
from scipy.spatial.qhull import ConvexHull
from scipy.spatial.distance import euclidean

res_kb = [100]
log_string = ''
EpiGfilename = 'http://www.lcqb.upmc.fr/meetu/dataforstudent/E116_15_coreMarks_dense'

for res in res_kb:
    if res >= 100:
        res_string = '100kb'
    elif 25 <= res < 100:
        res_string = '25kb'
    else:
        print('Invalid resolution value given')
        sys.exit(1)

    R = res * 100
    NbmaxEpi = 20  # Epi states go from 0 to 15 (compartments)
    alpha = 0.227  # what is this?
    selectedmark = 1  # index of the selected mark

    # Iterate over multiple chromosomes/files
    chromosomes = list(map(str, (range(2, 23))))
    chromosomes.append('X')
    for chromosome in chromosomes:
        HiCfilename = 'http://www.lcqb.upmc.fr/meetu/dataforstudent/HiC/GM12878/' + res_string + \
                      '_resolution_intrachromosomal/chr' + str(chromosome) + '_' + res_string + \
                      '.RAWobserved'

        # Build matrix
        A = np.loadtxt(HiCfilename)
        A = np.int_(A)
        print('Input data shape : ', np.shape(A))
        A = np.concatenate((A, np.transpose(np.array([A[:, 1], A[:, 0], A[:, 2]]))),
                           axis=0)  # build array at pb resolution
        A = sparse.coo_matrix((A[:, 2], (A[:, 0], A[:, 1])))
        binned_map = HiCtoolbox.bin2d(A, R, R)  # !become csr sparse array
        LENTEST = np.shape(A)[0]
        print('Input at the good resolution : ', np.shape(binned_map))

        del A  # keep space

        # Build color annotation at desired resolution
        color = pd.read_csv(EpiGfilename, delimiter='\t', header=None, names=[1, 2, 3, 4])
        color = color[color[1] == 'chr2']  # take only chr of interest
        number = color[4].max()  # number of color in the file
        color_vec = np.zeros((LENTEST, number + 1))  # build array at pb resolution LENchr * number of color

        i = 0
        while i < np.shape(color)[0]:
            color_vec[color[2].iloc[i]:color[3].iloc[i], color[4].iloc[i]] = 1
            i += 1

        color_bins = HiCtoolbox.bin2d(color_vec, R, 1)
        color_bins = color_bins / np.amax(color_bins)

        print('Bp cover by this mark, has to be >0 :', np.sum(color_bins[:, selectedmark]))

        # FILTER
        sumHicmat = np.sum(binned_map, 0)
        mini = np.mean(sumHicmat) - np.std(sumHicmat) * 1.5  # min value of filtering
        maxi = np.mean(sumHicmat) + np.std(sumHicmat) * 1.5  # max value of filtering
        binsaved = np.where(np.logical_and(mini < sumHicmat, sumHicmat < maxi))  # coord of bin to save
        filtered_map = binned_map[binsaved[1], :]  # save on raw
        filtered_map = filtered_map[:, binsaved[1]]  # save on col

        color2 = color_bins[binsaved[1]]  # filter the epi by removed bin in HiC
        color2 = color2[:, selectedmark]  # now color2 is 1D
        color2 = np.float64(color2.todense())  # type issue

        # 3D
        print('3D')  # Here : sparse int64
        contact_map = HiCtoolbox.SCN(filtered_map.copy())
        contact_map = np.asarray(contact_map) ** alpha  # now we are not sparse at all
        dist_matrix = HiCtoolbox.fastFloyd(1 / contact_map)  # shortest path on the matrix

        # Sammon
        [XYZ, E] = sammon.sammon(dist_matrix, 3)  # with the one from tom j pollard
        dist_matrix = dist_matrix - np.diag(np.diag(dist_matrix))  # remove the diagonal
        dist_matrix = (dist_matrix + np.transpose(
            dist_matrix)) / 2  # just to be sure that the matrix is symetric, not really useful in theory

        # O/E normalization
        N = len(dist_matrix)
        rg = range(N)
        diag_dict = {}
        for n in rg:
            diag_dict[n] = np.mean(np.diagonal(dist_matrix, offset=n, axis1=0, axis2=1))
            diag_dict[-n] = np.mean(np.diagonal(dist_matrix, offset=-n, axis1=0, axis2=1))
        for i in rg:
            for j in rg:
                offs = j - i
                if offs != 0:
                    dist_matrix[i, j] = dist_matrix[i, j] / diag_dict[offs]

        del binned_map, LENTEST, color, number, color_vec, color_bins, sumHicmat, mini, maxi, binsaved, \
            filtered_map, color2, contact_map
        cr = np.corrcoef(dist_matrix)
        cr -= np.mean(cr)
        eig_vals, eig_vects = np.linalg.eig(cr)


        def scoring_from_eigen(eig_vals: np.array,
                               eig_vects: np.array,
                               predictions: np.array,
                               comps: int,
                               method: str = 'bomba'):
            score = 0
            if method == 'slimak':
                for ci in range(comps):
                    pts = np.array(
                        [eig_vals[i] * eig_vects[i] for i in range(eig_vects.shape[0]) if predictions[i] == ci])
                    score += np.sum([euclidean(pts[i], pts[i + 1]) for i in range(pts.shape[0] - 1)])
                return 1 / score
            elif method == 'bomba':
                for ci in range(comps):
                    pts = np.array(
                        [eig_vals[i] * eig_vects[i] for i in range(eig_vects.shape[0]) if predictions[i] == ci])
                    pts = PCA(n_components=pts.shape[0] - 1).fit_transform(pts)
                    hull = ConvexHull(pts)
                    vertices = hull.vertices.tolist() + [hull.vertices[0]]
                    perimeter = np.sum([euclidean(x, y) for x, y in zip(pts[vertices], pts[vertices][1:])])
                    score += perimeter
                return 1 / score
            raise ValueError('method is missing')


        comps = range(2, NbmaxEpi + 1)
        best_comp = None
        bc_score = 0
        predictions2c = None
        predictions5c = None
        for c in comps:
            model = hmm.GaussianHMM(n_components=c, n_iter=1000, covariance_type="diag")
            model.fit(cr)
            predictions = model.predict(cr)
            if c == 2:
                predictions2c = predictions
            elif c == 5:
                predictions5c = predictions
            # for 'avg' returns always the same - that is bad
            score = scoring_from_eigen(eig_vals, eig_vects, predictions, c)
            if score > bc_score:
                best_comp = predictions
                bc_score = score
            log_string = log_string + 'comp' + str(c) + ' done, score ' + str(score) + '\n'
            print('comp' + str(c) + ' done, score ' + str(score) + '\n')
        del dist_matrix, cr
        best_comp_N = len(np.unique(best_comp))
        log_string = log_string + 'best component N for chromosome ' + chromosome + ' at resolution ' + \
                     str(res) + 'kb is ' + str(best_comp_N) + '\n'

        x = XYZ[:, 0]
        y = XYZ[:, 1]
        z = XYZ[:, 2]

        cols5c = predictions5c / (predictions5c.max() / 255)
        cols2c = predictions2c / (predictions2c.max() / 255)
        colsbestc = best_comp / (best_comp.max() / 255)
        # best for each chromosome
        fig5c = plt.figure(figsize=[15, 15])
        ax = fig5c.gca(projection='3d')
        ax.scatter(x.ravel(),
                   y.ravel(),
                   z.ravel(),
                   c=cols5c,
                   cmap="gist_rainbow")
        fig5c.savefig('5c_at' + chromosome + '_chromosome_' + str(res) + 'kb')
        # 2c
        fig2c = plt.figure(figsize=[15, 15])
        ax = fig2c.gca(projection='3d')
        ax.scatter(x.ravel(),
                   y.ravel(),
                   z.ravel(),
                   c=cols2c,
                   cmap="gist_rainbow")
        fig2c.savefig('2c_at' + chromosome + '_chromosome_' + str(res) + 'kb')
        figbestc = plt.figure(figsize=[15, 15])
        ax = figbestc.gca(projection='3d')
        ax.scatter(x.ravel(),
                   y.ravel(),
                   z.ravel(),
                   c=colsbestc,
                   cmap="gist_rainbow")
        figbestc.savefig(str(best_comp_N) + '_akaBESTcompN_at_' + chromosome + '_chromosome_' + str(res) + 'kb')

text_file = open("log.txt", "w")
text_file.write(log_string)
text_file.close()
